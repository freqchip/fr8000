/**
 * Copyright (c) 2019, Freqchip
 * 
 * All rights reserved.
 * 
 * 
 */
 
 /*
 * INCLUDES (包含头文件)
 */
#include <stdio.h>
#include <string.h>

#include "co_log.h"

#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"

/*
 * MACROS (宏定义)
 */
#undef LOG_LOCAL_LEVEL
#define LOG_LOCAL_LEVEL        (LOG_LEVEL_INFO)
extern const char *app_tag;

/*
 * CONSTANTS (常量定义)
 */

#define SP_CHAR1_UUID            0xFFF1
#define SP_CHAR2_UUID            0xFFF2
#define SP_CHAR3_UUID            0xFFF4

#if 1
const gatt_uuid_t client_att_tb[] =
{
    [0]  =
    { UUID_SIZE_2, UUID16_ARR(SP_CHAR1_UUID)},
    [1]  =
    { UUID_SIZE_2, UUID16_ARR(SP_CHAR2_UUID)},
		[2]  =
    { UUID_SIZE_2, UUID16_ARR(SP_CHAR3_UUID)},
		
};
#else
uint16_t handles[2] = {0,0};

gatt_uuid_t client_att_tb[2] = {0};
#endif


/*
 * TYPEDEFS (类型定义)
 */

/*
 * GLOBAL VARIABLES (全局变量)
 */
uint8_t client_id;

uint8_t master_link_conidx;

/*
 * LOCAL VARIABLES (本地变量)
 */

 
/*
 * LOCAL FUNCTIONS (本地函数)
 */
static void simple_central_start_scan(void);

/*********************************************************************
 * @fn      app_gap_evt_cb
 *
 * @brief   Application layer GAP event callback function. Handles GAP evnets.
 *
 * @param   p_event - GAP events from BLE stack.
 *       
 *
 * @return  None.
 */
static void app_gap_evt_cb(gap_event_t *p_event)
{
	LOG_INFO(app_tag, "p_event->type:0x%02x\r\n",p_event->type);
    switch(p_event->type)
    {
        case GAP_EVT_SCAN_END:
            LOG_INFO(app_tag, "scan_end,status:0x%02x\r\n",p_event->param.scan_end_status);
            break;
 
        case GAP_EVT_ADV_REPORT:
        {
            uint8_t scan_name[] = "Simple Peripheral";

//            if(memcmp(p_event->param.adv_rpt->src_addr.addr.addr,"\xbd\xad\x10\x11\x20\x20",6)==0)
            if (p_event->param.adv_rpt->data[0] == 0x12
                && p_event->param.adv_rpt->data[1] == GAP_ADVTYPE_LOCAL_NAME_COMPLETE
                && memcmp(&(p_event->param.adv_rpt->data[2]), scan_name, 0x10) == 0)
            {
                LOG_INFO(app_tag, "scan_addr:%02x%02x%02x%02x%02x%02x",	p_event->param.adv_rpt->src_addr.addr.addr[0],\
                                                                p_event->param.adv_rpt->src_addr.addr.addr[1],\
                                                                p_event->param.adv_rpt->src_addr.addr.addr[2],\
                                                                p_event->param.adv_rpt->src_addr.addr.addr[3],\
                                                                p_event->param.adv_rpt->src_addr.addr.addr[4],\
                                                                p_event->param.adv_rpt->src_addr.addr.addr[5]);
                gap_stop_scan();
                
                LOG_INFO(app_tag, "evt_type:0x%02x,rssi:%d\r\n",p_event->param.adv_rpt->evt_type,p_event->param.adv_rpt->rssi);
                
                gap_start_conn(&(p_event->param.adv_rpt->src_addr.addr),
                                p_event->param.adv_rpt->src_addr.addr_type,
                                30, 30, 0, 300);
            }

        }
        break;

        case GAP_EVT_MASTER_CONNECT:
        {
            LOG_INFO(app_tag, "master[%d],connect. link_num:%d\r\n",p_event->param.master_connect.conidx,gap_get_connect_num());
            master_link_conidx = (p_event->param.master_connect.conidx);
            LOG_INFO(app_tag, "gap_security_get_bond_status:%d\r\n",gap_security_get_bond_status());
            if (gap_security_get_bond_status())
                gap_security_enc_req(p_event->param.master_connect.conidx);
            else
                gap_security_pairing_req(p_event->param.master_connect.conidx);

        }
        break;

        case GAP_EVT_DISCONNECT:
        {
					
            //gap_bond_manager_info_clr("\x0C\x0C\x0C\x0C\x0C\x0B", 0);
            LOG_INFO(app_tag, "Link[%d] disconnect,reason:0x%02X\r\n",p_event->param.disconnect.conidx
                      ,p_event->param.disconnect.reason);
					simple_central_start_scan();
        }
        break;

        case GAP_EVT_LINK_PARAM_REJECT:
            LOG_INFO(app_tag, "Link[%d]param reject,status:0x%02x\r\n"
                      ,p_event->param.link_reject.conidx,p_event->param.link_reject.status);
            break;

        case GAP_EVT_LINK_PARAM_UPDATE:
            LOG_INFO(app_tag, "Link[%d]param update,interval:%d,latency:%d,timeout:%d\r\n",p_event->param.link_update.conidx
                      ,p_event->param.link_update.con_interval,p_event->param.link_update.con_latency,p_event->param.link_update.sup_to);
            break;

        case GAP_EVT_CONN_END:
            LOG_INFO(app_tag, "conn_end,reason:0x%02x\r\n",p_event->param.conn_end_reason);
					
            break;

        case GAP_EVT_PEER_FEATURE:
            LOG_INFO(app_tag, "peer[%d] feats ind\r\n",p_event->param.peer_feature.conidx);
            break;

        case GAP_EVT_MTU:
            LOG_INFO(app_tag, "mtu update,conidx=%d,mtu=%d\r\n"
                      ,p_event->param.mtu.conidx,p_event->param.mtu.value);
            break;
        case GAP_EVT_LINK_RSSI:
            LOG_INFO(app_tag, "link rssi %d\r\n",p_event->param.link_rssi);
            break;

        case GAP_SEC_EVT_MASTER_ENCRYPT:
            LOG_INFO(app_tag, "master[%d]_encrypted\r\n",p_event->param.master_encrypt_conidx);
            extern uint8_t client_id;
            gatt_discovery_all_peer_svc(client_id,p_event->param.master_encrypt_conidx);
            //uint8_t group_uuid[] = {0xb7, 0x5c, 0x49, 0xd2, 0x04, 0xa3, 0x40, 0x71, 0xa0, 0xb5, 0x35, 0x85, 0x3e, 0xb0, 0x83, 0x07};
            //gatt_discovery_peer_svc(client_id,event->param.master_encrypt_conidx,16,group_uuid);
            break;
        case GAP_SEC_EVT_MASTER_ENCRYPT_FAIL:
            LOG_INFO(app_tag, "master_encrypt_fail:%02x %02x\r\n",p_event->param.master_encrypt_fail.reason,p_event->param.master_encrypt_fail.conidx);
           
            break;

        default:
            break;
    }
}

/*********************************************************************
 * @fn      simple_central_start_scan
 *
 * @brief   Set central role scan parameters and start scanning BLE devices.
 *
 * @param   None. 
 *       
 *
 * @return  None.
 */
static void simple_central_start_scan(void)
{
    // Start Scanning
    LOG_INFO(app_tag, "Start scanning...\r\n");
    gap_scan_param_t scan_param;
    scan_param.scan_mode = GAP_SCAN_MODE_GEN_DISC;
    scan_param.dup_filt_pol = 0;
    scan_param.scan_intv = 32;  //scan event on-going time
    scan_param.scan_window = 20;
    scan_param.duration = 0;
    gap_start_scan(&scan_param);
}

/*********************************************************************
 * @fn      simple_central_msg_handler
 *
 * @brief   Simple Central GATT message handler, handles messages from GATT layer.
 *          Messages like read/write response, notification/indication values, etc.
 *
 * @param   p_msg       - GATT message structure.
 *
 * @return  uint16_t    - Data length of the GATT message handled.
 */
static uint16_t simple_central_msg_handler(gatt_msg_t *p_msg)
{
    LOG_INFO(app_tag, "CCC:%x\r\n",p_msg->msg_evt);
    switch(p_msg->msg_evt)
    {
        case GATTC_MSG_NTF_REQ:
        {
            LOG_INFO(app_tag, "GATTC_MSG_NTF_REQ\r\n");
        }
        break;
        case GATTC_MSG_READ_REQ:
        {
            LOG_INFO(app_tag, "GATTC_MSG_READ_REQ:%d\r\n",p_msg->att_idx);
        }
        break;
        case GATTC_MSG_READ_IND:
        {
            LOG_INFO(app_tag, "GATTC_MSG_READ_IND\r\n");
        }
        break;
        case GATTC_MSG_SVC_REPORT:
        {
            gatt_svc_report_t *svc_rpt = (gatt_svc_report_t *)(p_msg->param.msg.p_msg_data);
            LOG_INFO(app_tag, "svc:%d,start_hdl:%d,end_hdl:%d\r\n",svc_rpt->uuid_len,svc_rpt->start_hdl,svc_rpt->end_hdl);
        }
        break;
        
        case GATTC_MSG_CMP_EVT:
        {
            LOG_INFO(app_tag, "op:%d done\r\n",p_msg->param.op.operation);
            if(p_msg->param.op.operation == GATT_OP_PEER_SVC_REGISTERED)
            {
                LOG_INFO(app_tag, "client_id:%d %d\r\n",client_id,p_msg->conn_idx);
                uint16_t att_handles[2];
                memcpy(att_handles,p_msg->param.op.arg,4);

                gatt_client_enable_ntf_t ntf_enable;
                ntf_enable.conidx = p_msg->conn_idx;
                ntf_enable.client_id = client_id;
                ntf_enable.att_idx = 2; //NF
                gatt_client_enable_ntf(ntf_enable);

               	gatt_client_read_t read;
                read.conidx = p_msg->conn_idx;
                read.client_id = client_id;
                read.att_idx = 1; //TX
                gatt_client_read(read);
							
							
                gatt_client_write_t write;
                write.conidx = p_msg->conn_idx;
                write.client_id = client_id;
                write.att_idx = 0; //RX
                write.p_data = "\x1\x2\x3\x4\x5\x6\x7";
                write.data_len = 7;
                gatt_client_write_cmd(write);
            }
        }
        break;
        
        default:
        break;
    }

    return 0;
}

/*
 * EXTERN FUNCTIONS (外部函数)
 */

/*
 * PUBLIC FUNCTIONS (全局函数)
 */

/** @function group ble peripheral device APIs (ble外设相关的API)
 * @{
 */

/*********************************************************************
 * @fn      simple_central_init
 *
 * @brief   Initialize simple central, BLE related parameters.
 *
 * @param   None. 
 *       
 *
 * @return  None.
 */
void simple_central_init(void)
{
    // set local device name
    uint8_t local_name[] = "Simple Central";
    gap_dev_name_set(local_name, sizeof(local_name));

    gap_set_cb_func(app_gap_evt_cb);
    
    // Initialize security related settings.
    gap_bond_manager_delete_all();    

    gap_security_param_t param =
    {
        .mitm = false,
        .ble_secure_conn = false,
        .io_cap = GAP_IO_CAP_NO_INPUT_NO_OUTPUT,
        .pair_init_mode = GAP_PAIRING_MODE_WAIT_FOR_REQ,
        .bond_auth = true,
        .password = 0,
    };

    gap_security_param_init(&param);
    
    // Initialize GATT 
    gatt_client_t client;
    
    client.p_att_tb = client_att_tb;
    client.att_nb = 3;
    client.gatt_msg_handler = simple_central_msg_handler;
    client_id = gatt_add_client(&client);
    
    simple_central_start_scan();
}
