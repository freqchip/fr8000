/*
  ******************************************************************************
  * @file    pdm_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of pdm module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __PDM_DEMO_H__
#define __PDM_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* PDM demo select */
typedef enum
{
    PDM_QUERY_MODE,   /* Read data in Query Mode */
    PDM_INT_MODE,     /* Read data in interrupt mode */ 
}enum_PDM_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* pdm_demo */
void pdm_demo(enum_PDM_Demo_t fe_Demo);

#endif
