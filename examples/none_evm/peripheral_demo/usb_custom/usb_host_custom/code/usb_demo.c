/*
  ******************************************************************************
  * @file    usb_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   USB module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "usb_demo.h"

#include "driver_gpio.h"

#include "co_printf.h"
#include "sys_utils.h"

#include "usb_cdc.h"

GPIO_InitTypeDef GPIO_Init;
uint8_t lu8_RxCount;

uint8_t Endpoint1_TxBuffer[64];
uint8_t Endpoint1_RxBuffer[64];
volatile uint32_t Connect_flag;

/*********************************************************************
 * @fn      Transparentmission_Data_Init
 *
 * @brief   Transparent transmission Data initialization 
 *
 * @param   None.
 * @return  None.
 */
void Transparentmission_Data_Init(void)
{
    uint32_t i;

    for (i = 0; i < 64; i++)
    {
        if (i < 32)
        {
            Endpoint1_TxBuffer[i] = 0x11;
        }
        else
        {
            Endpoint1_TxBuffer[i] = i;
        }
    }
}

/*********************************************************************
 * @fn      USB_Connect_flag
 *
 * @brief   USB Connect interact Flag
 *
 * @param   None.
 * @return  None.
 */

void USB_Connect_flag(void)
{
    Connect_flag = 1;   
}

/*********************************************************************
 * @fn      usb_host_custom
 *
 * @brief   User host customization
 *
 * @param   None.
 * @return  None.
 */
void usb_host_custom(void)
{
    __SYSTEM_GPIO_CLK_ENABLE();
    
    Transparentmission_Data_Init();

	NVIC_ClearPendingIRQ(USBMCU_IRQn);
	NVIC_EnableIRQ(USBMCU_IRQn);
    
    GPIO_Init.Alternate = GPIO_FUNCTION_5;
    GPIO_Init.Mode = GPIO_MODE_AF_PP; 
    GPIO_Init.Pin  = GPIO_PIN_0 | GPIO_PIN_1;
    GPIO_Init.Pull = GPIO_NOPULL;   
    gpio_init(GPIO_B, &GPIO_Init);
    
    USB_Connect_Handler = USB_Connect_flag;
    
    usb_host_init();
    
    usb_selecet_endpoint(ENDPOINT_1);   // 64Byte
    usb_endpoint_Txfifo_config(64/8, 3);
    usb_endpoint_Rxfifo_config(128/8, 3);
    usb_TxMaxP_set(8);
    usb_RxMaxP_set(8);
       
    while(!Connect_flag);    
    
    co_delay_10us(40000);   
    usb_Host_ResetSignalStart();
    co_delay_10us(1000 * 2);
    usb_Host_ResetSignalStop();

    for(int i = 0; i < 3; i++)
    {
        /* --------------------------- endpoint1 --------------------------- */
        usb_selecet_endpoint(ENDPOINT_1);
        usb_write_fifo(ENDPOINT_1, Endpoint1_TxBuffer, 64);
        usb_Host_TxTargetEndpoint(ENDPOINT_1);
        usb_Host_Endpoints_SET_TxPktRdy();
        while(usb_Host_Endpoints_GET_TxPktRdy());
        usb_Endpoints_FlushTxFIFO();

        usb_Host_RxTargetEndpoint(ENDPOINT_1);
        usb_Host_Endpoints_SET_RxReqPkt();
        while(!usb_Host_Endpoints_GET_RxPktRdy());
        lu8_RxCount = usb_Endpoints_get_RxCount();
        usb_read_fifo(ENDPOINT_1, Endpoint1_RxBuffer, lu8_RxCount);
        usb_Endpoints_FlushRxFIFO();
        
        //Data check
        co_printf("Transparent transmission Data Count: %d\r\n",lu8_RxCount);        
        for(int i = 0; i < lu8_RxCount; i++)
        {
             if(Endpoint1_RxBuffer[i] != Endpoint1_TxBuffer[i])
             {
                  co_printf("check err\r\n");
             }
        } 
        memset(Endpoint1_RxBuffer,0,sizeof(Endpoint1_RxBuffer));        
    }
    
    co_printf("Transparent transmission end\r\n");  
    while(1);
}    


