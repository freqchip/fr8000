/*
  ******************************************************************************
  * @file    wdt_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of wdt_demo module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __WDT_DEMO_H__
#define __WDT_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* WDT demo select */
typedef enum
{
	WDT_WITHOUT_INTERRUPT,
	WDT_WITH_INTERRUPT,
}enum_WDT_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* wdt_demo */
void wdt_demo(enum_WDT_Demo_t fe_Demo);

#endif
