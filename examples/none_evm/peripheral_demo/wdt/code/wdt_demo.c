/*
  ******************************************************************************
  * @file    wdt_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   wdt module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "wdt_demo.h"

#include "driver_pmu.h"
#include "driver_wdt.h"

#include "co_printf.h"
#include "co_log.h"

#include "ble_stack.h"

wdt_Init_t wdt_handle;

/************************************************************************************
 * @fn      timer_demo
 *
 * @brief   timer demo
 */
void wdt_demo(enum_WDT_Demo_t fe_Demo)
{
    co_printf("this is wdt_demo \r\n");
    
    switch (fe_Demo)
    {
        case WDT_WITHOUT_INTERRUPT:
        {
            /*
                brief description:
                First, The watchdog count decrement to 0. 
                And then the Timeout decrement to 0.
                If wdt_Refresh is not called, the system will reset.
            */
            wdt_handle.IRQ_Enable = WDT_IRQ_DISABLE;
            wdt_handle.Timeout    = 0xFF;
            wdt_handle.WdtCount   = 32000 * 5;     // 32K, timeout 5s
            wdt_init(wdt_handle);
        }break;
        
        case WDT_WITH_INTERRUPT:
        {
            /*
                brief description:
                First, The watchdog count decrement to 0 trigger PMU_WDT interrupt. 
                And then the Timeout decrement to 0.
                If wdt_Refresh is not called, the system will reset.
            */
            wdt_handle.IRQ_Enable = WDT_IRQ_ENABLE;
            wdt_handle.Timeout    = 0xFF;
            wdt_handle.WdtCount   = 32000 * 5;     // 32K, timeout 5s
            wdt_init(wdt_handle);

            /* wdt interrupt enable */
            pmu_enable_isr(PMU_WTD_INT_EN);
        }break;
        
        default: break;
    }


    while(1);
}
