/*
  ******************************************************************************
  * @file    driver_gpio.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of GPIO module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __GPIO_DEMO_H__
#define __GPIO_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* GPIO demo select */
typedef enum
{
	GPIO_EXTI_INT,
	GPIO_WAKE_UP,
    GOIO_OUTPUT,
    GPIO_INPUT,
}enum_GPIO_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* gpio_demo */
void gpio_demo(enum_GPIO_Demo_t fe_Demo);

#endif
