/*
  ******************************************************************************
  * @file    gpio_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   GPIO module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "gpio_demo.h"

#include "driver_gpio.h"
#include "driver_pmu.h"

#include "co_printf.h"
#include "co_log.h"

#include "ble_stack.h"

void main_loop(void);

uint8_t Pin_value;
GPIO_InitTypeDef GPIO_Handle;

/************************************************************************************
 * @fn      gpio_demo
 *
 * @brief   gpio demo
 *
 * @param   fe_Demo: demo select. 

 */
void gpio_demo(enum_GPIO_Demo_t fe_Demo)
{
    __SYSTEM_GPIO_CLK_ENABLE();

	switch(fe_Demo)
	{
        case GOIO_OUTPUT:
        {
            // gpio output
			GPIO_Handle.Pin       = GPIO_PIN_0|GPIO_PIN_1;
			GPIO_Handle.Mode      = GPIO_MODE_OUTPUT_PP;
			GPIO_Handle.Pull      = GPIO_PULLUP;
            
			gpio_init(GPIO_B, &GPIO_Handle);   

            while(1)
            {
                //pin Output high Level 
                gpio_write_pin(GPIO_B, GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_SET);
                co_delay_10us(100);
                
                //pin Output low Level 
                gpio_write_pin(GPIO_B, GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_CLEAR); 
                co_delay_10us(100);               
            }
        }
        
        case GPIO_INPUT:
        {
            // gpio input
			GPIO_Handle.Pin       = GPIO_PIN_0|GPIO_PIN_1;
			GPIO_Handle.Mode      = GPIO_MODE_INPUT;
			GPIO_Handle.Pull      = GPIO_PULLUP;
            
			gpio_init(GPIO_B, &GPIO_Handle); 
            
            while(1)
            {
                Pin_value = gpio_read_pin(GPIO_B, GPIO_PIN_0);
                co_printf("portb0 pin value is %d\r\n",Pin_value);
                co_delay_10us(10000);                
                
                Pin_value = gpio_read_pin(GPIO_B, GPIO_PIN_1);
                co_printf("portb1 pin value is %d\r\n",Pin_value);
                co_delay_10us(10000);               
            }
        }
        
		case GPIO_EXTI_INT:
		{
			// EXTI interrupt
			GPIO_Handle.Pin       = GPIO_PIN_0|GPIO_PIN_1;
			GPIO_Handle.Mode      = GPIO_MODE_EXTI_IT_FALLING;
			GPIO_Handle.Pull      = GPIO_PULLUP;

			gpio_init(GPIO_B, &GPIO_Handle);

			exti_interrupt_enable(EXTI_LINE8_PB0);
			exti_interrupt_enable(EXTI_LINE9_PB1);
			
			exti_clear_LineStatus(EXTI_LINE8_PB0);
			exti_clear_LineStatus(EXTI_LINE9_PB1);
			
			NVIC_EnableIRQ(GPIO_IRQn);

			while (1);
		}
		
		case GPIO_WAKE_UP:
		{
			pmu_port_wakeup_func_set(GPIO_PORT_B, GPIO_PIN_0);

			NVIC_EnableIRQ(PMU_IRQn);

			/* system sleep is allowed */
			system_sleep_enable();
			
			while(1)
			{
				main_loop();
			}
		}
	}
}


/************************************************************************************
 * @fn      exti_isr
 *
 * @brief   exti interrupt handler
 */
void exti_isr(void)
{
	if (exti_get_LineStatus(EXTI_LINE8_PB0))
	{
		exti_clear_LineStatus(EXTI_LINE8_PB0);
		
		co_printf("this is exti line 8 \r\n");
	}

	if (exti_get_LineStatus(EXTI_LINE9_PB1))
	{
		exti_clear_LineStatus(EXTI_LINE9_PB1);
		
		co_printf("this is exti line 9 \r\n");
	}
}
