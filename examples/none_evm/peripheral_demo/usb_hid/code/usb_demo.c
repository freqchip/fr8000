/*
  ******************************************************************************
  * @file    usb_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   USB module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "usb_demo.h"

#include "driver_gpio.h"
#include "driver_spi.h"
#include "driver_system.h"
#include "driver_pmu.h"

#include "co_printf.h"
#include "co_log.h"
#include "sys_utils.h"
#include "ble_stack.h"

#include "usb_hid.h"

/************************************************************************************
 * @fn      usb_demo
 *
 * @brief   usb demo
 */
void usb_demo(void)
{
    pmu_usb_pad_ctrl(true);
    
	NVIC_ClearPendingIRQ(USBMCU_IRQn);
	NVIC_SetPriority(USBMCU_IRQn, 0);
	NVIC_EnableIRQ(USBMCU_IRQn);

	usb_device_init();
	usb_hid_init();

	usb_DP_Pullup_Enable();

	while(1)
	{
		
	}
}

