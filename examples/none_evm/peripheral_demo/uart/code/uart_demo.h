/*
  ******************************************************************************
  * @file    uart_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of uart module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __UART_DEMO_H__
#define __UART_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* uart demo select */
typedef enum
{
    UART_TRANSMIT_RECEIVE,
    UART_TRANSMIT_RECEIVE_IT,
    UART_TRANSMIT_RECEIVE_DMA,    
}enum_uart_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* uart_demo */
void uart_demo(enum_uart_Demo_t uart_Demo);

#endif
