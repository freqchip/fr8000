/*
  ******************************************************************************
  * @file    KeyScan_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2023
  * @brief   Header file of KeyScan module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __KEYSCAN_DEMO_H__
#define __KEYSCAN_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/* KeyScan_demo */
void KeyScan_demo(void);

#endif
