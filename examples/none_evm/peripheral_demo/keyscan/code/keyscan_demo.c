/*
  ******************************************************************************
  * @file    KeyScan_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2023
  * @brief   KeyScan module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "keyscan_demo.h"
#include "driver_pmu.h"
#include "co_printf.h"
#include "driver_keyscan.h"
#include "co_log.h"

str_KeyScanParam_t  KeyScanParam;
/************************************************************************************
 * @fn      KeyScan_demo
 *
 * @brief   KeyScan demo
 *
 * @param   fe_Demo: demo select. 

 */

void KeyScan_demo(void)
{ 
    /*
        ScanInterval : 0xF
        FilterValue  : 0x05
        ROW : PD0 PD1 PD2 PD3 PD4 PD5 PD6 PD7
        COL : PC3~PC0 PB7~PB0 PA7~PA2 PA0 
              0xFFFFD
    */
    KeyScanParam.RowSelect_Port = ROW_PORT_D;
    KeyScanParam.RowSelect_Pin  = 0xFF;
    KeyScanParam.ColSelect_Pin_GroupA = 0xFFFFD;   
    KeyScanParam.ScanInterval   = 0xF;
    KeyScanParam.FilterValue    = 0x05;
    keyscan_init(KeyScanParam);
    
    NVIC_EnableIRQ(PMU_IRQn);
    
    while(1);
}

static uint32_t KeyValue[5];

void pmu_keyscan_isr(void)
{
    keyscan_IRQHandler();
    
    keyscan_ReadKeyValue(KeyValue);
    printf("0x%08X \r\n", KeyValue[0]);
    printf("0x%08X \r\n", KeyValue[1]);
    printf("0x%08X \r\n", KeyValue[2]);
    printf("0x%08X \r\n", KeyValue[3]);
    printf("0x%08X \r\n", KeyValue[4]);
}
