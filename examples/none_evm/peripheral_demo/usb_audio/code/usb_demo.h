/*
  ******************************************************************************
  * @file    usb_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of USB module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __USB_DEMO_H__
#define __USB_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/* usb_demo */
void usb_demo(void);

#endif
