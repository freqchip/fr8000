/*
  ******************************************************************************
  * @file    pwm_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   PWM module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "pwm_demo.h"

#include "driver_pwm.h"
#include "driver_gpio.h"

#include "driver_system.h"
#include "co_printf.h"

/************************************************************************************
 * @fn      pwm_demo
 *
 * @brief   test pwm function
 *
 * @param   Pwm_Demo : select test model
 */
void pwm_demo(enum_PWM_Demo_t Pwm_Demo)
{
    GPIO_InitTypeDef GPIO_Handle;
    struct_PWM_Config_t PWM_Config;
    struct_PWM_DAC_Config_t DAC_Config;
    struct_Capture_Config_t Capture_Config;
    struct_PWM_Complementary_Config_t Complementary_Config;
    
    __SYSTEM_PWM_CLK_ENABLE();  

    /* init GPIO Alternate Function */
    GPIO_Handle.Pin       = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
    GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
    GPIO_Handle.Pull      = GPIO_PULLUP;
    GPIO_Handle.Alternate = GPIO_FUNCTION_6;

    gpio_init(GPIO_D, &GPIO_Handle);
    
    switch(Pwm_Demo)
    {
        case PWM_MODE:
        {
            //--------------------------//
            //         PWM Mode         //
            //--------------------------//
            PWM_Config.Prescale = 1;
            PWM_Config.Period   = 50;
            PWM_Config.Posedge  = 0;
            PWM_Config.Negedge  = 25;
            pwm_config(PWM_CHANNEL_0|PWM_CHANNEL_1|PWM_CHANNEL_2, PWM_Config);

            pwm_output_enable(PWM_CHANNEL_0|PWM_CHANNEL_1|PWM_CHANNEL_2);
            co_printf("pwm model\r\n");
        
        }break;
        
        case PWM_CPMPLEMENTARY:
        {
            //--------------------------//
            //    Complementary Mode    //
            //--------------------------//
            Complementary_Config.Prescale = 48;
            Complementary_Config.Period   = 10;
            Complementary_Config.DutyCycle = 5;
            Complementary_Config.MianDeadTime = 1;
            Complementary_Config.CompDeadTime = 1;
            pwm_complementary_config(PWM_CHANNEL_0, PWM_CHANNEL_1, Complementary_Config);
            
            pwm_output_enable(PWM_CHANNEL_0|PWM_CHANNEL_1);
            co_printf("pwm Complementary model\r\n");
        }break;

        case PWM_DAC_MODE:
        {
            //--------------------------//
            //       PWM DAC Mode       //
            //--------------------------//
            DAC_Config.PulseDIV = 11;
            DAC_Config.Duty = 50;
            pwm_dac_config(PWM_CHANNEL_3|PWM_CHANNEL_4, DAC_Config);

            pwm_output_enable(PWM_CHANNEL_3|PWM_CHANNEL_4);
            co_printf("pwm dac model\r\n");
        }break;

        case CAPTURE_MODE:
        {
            //--------------------------//
            //       Capture Mode       //
            //--------------------------//
            Capture_Config.CapturePrescale = CAPTURE_DIV_1;
            Capture_Config.CaptureMode = MODE_LOOP;
            pwm_capture_config(PWM_CHANNEL_5, Capture_Config);
            
            pwm_capture_enable(PWM_CHANNEL_5);
            co_printf("capture model\r\n");
            while (1) 
            {
                if (pwm_capture_status(PWM_CHANNEL_5)) 
                {
                    co_printf("0x%08X \r\n", pwm_capture_value(PWM_CHANNEL_5));
                    pwm_capture_status_clear(PWM_CHANNEL_5);
                }
            }
        }         
        default:
            break;
    }  
    while(1);
}
