/*
  ******************************************************************************
  * @file    pwm_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of PWM module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __PWM_DEMO_H__
#define __PWM_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

#include "plf.h"

/* PWM demo select */
typedef enum
{
	PWM_MODE,
    PWM_CPMPLEMENTARY,
	PWM_DAC_MODE,
    CAPTURE_MODE,
}enum_PWM_Demo_t;

void pwm_demo(enum_PWM_Demo_t Pwm_Demo);

#endif
