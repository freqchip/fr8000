/*
  ******************************************************************************
  * @file    timer_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of timer_demo module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __TIMER_DEMO_H__
#define __TIMER_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/* timer_demo */
void timer_demo(void);

#endif
