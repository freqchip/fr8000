/*
  ******************************************************************************
  * @file    timer_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Timer module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "timer_demo.h"

#include "driver_pmu.h"
#include "driver_timer.h"

#include "co_printf.h"
#include "co_log.h"

#include "ble_stack.h"

/************************************************************************************
 * @fn      timer_demo
 *
 * @brief   timer demo
 */
void timer_demo(void)
{
    __SYSTEM_TIMER_CLK_ENABLE();

    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_EnableIRQ(TIMER0_IRQn);

    NVIC_ClearPendingIRQ(TIMER1_IRQn);
    NVIC_EnableIRQ(TIMER1_IRQn);
    
    /* timer0 1s */
    timer_init(Timer0, 96000000, TIMER_DIV_NONE);
    /* timer1 2s */
    timer_init(Timer1, 96000000 * 2, TIMER_DIV_NONE);
    
    timer_start(Timer0);
    timer_start(Timer1);
    
    while(1);
}

/************************************************************************************
 * @fn      timer0_handler
 *
 * @brief   timer0 interrupt handler
 */
void timer0_isr(void)
{
    static uint32_t lu32_Count = 0;
    
    timer_int_clear(Timer0);
    
    co_printf("timer0 count:%d \r\n", lu32_Count++);
}

/************************************************************************************
 * @fn      timer1_handler
 *
 * @brief   timer1 interrupt handler
 */
void timer1_isr(void)
{
    static uint32_t lu32_Count = 0;
    
    timer_int_clear(Timer1);
    
    co_printf("timer1 count:%d \r\n", lu32_Count++);
}
