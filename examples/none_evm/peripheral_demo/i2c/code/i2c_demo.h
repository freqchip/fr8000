/*
  ******************************************************************************
  * @file    i2c_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of I2C demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __I2C_DEMO_H__
#define __I2C_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/* i2c_demo */
void i2c_demo(void);

#endif
