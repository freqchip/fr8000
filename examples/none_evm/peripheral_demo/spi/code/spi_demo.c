/*
  ******************************************************************************
  * @file    spi_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   SPI module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "spi_demo.h"

#include "driver_gpio.h"
#include "driver_spi.h"
#include "driver_system.h"
#include "IC_W25Qxx.h"

#include "co_printf.h"
#include "co_log.h"
#include "sys_utils.h"
#include "ble_stack.h"

GPIO_InitTypeDef   GPIO_Handle;
SPI_HandleTypeDef  SPI0_Handle;

uint8_t TxBufferI2C[256];
uint8_t RxBufferI2C[256];

/************************************************************************************
 * @fn      spi_demo
 *
 * @brief   spi demo
 */
void spi_demo(void)
{
	uint32_t i;
	uint16_t lu16_FlashID;

    for (i = 0; i < 256; i++)
	{
	    TxBufferI2C[i] = i;
	}
	
	__SYSTEM_GPIO_CLK_ENABLE();
	__SYSTEM_SPI0_MASTER_CLK_ENABLE();
	__SYSTEM_SPI0_MASTER_CLK_SELECT_96M();

	/* SPI IO Init */
	/* PA6 --- CLK */
	/* PA7 --- CS  */
	/* PA2 --- IO0 */
	/* PA3 --- IO1 */
	/* PA4 --- IO2 */
	/* PA5 --- IO3 */
    GPIO_Handle.Pin       = GPIO_PIN_6 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
    GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
    GPIO_Handle.Pull      = GPIO_PULLUP;
    GPIO_Handle.Alternate = GPIO_FUNCTION_2;
    gpio_init(GPIO_A, &GPIO_Handle);
	/* PA7 --- CS: software control */
    GPIO_Handle.Pin       = GPIO_PIN_7;
    GPIO_Handle.Mode      = GPIO_MODE_OUTPUT_PP;
    gpio_init(GPIO_A, &GPIO_Handle);

    SPI0_Handle.SPIx                       = SPIM0;
    SPI0_Handle.Init.Work_Mode             = SPI_WORK_MODE_0;
    SPI0_Handle.Init.Frame_Size            = SPI_FRAME_SIZE_8BIT;
    SPI0_Handle.Init.BaudRate_Prescaler    = 100;
    SPI0_Handle.Init.TxFIFOEmpty_Threshold = 20;
    SPI0_Handle.Init.RxFIFOFull_Threshold  = 0;
    spi_master_init(&SPI0_Handle);

	__SPI_CS_Release();
	
	lu16_FlashID = IC_W25Qxx_Read_ID();
	co_printf("Flash_ID: %04X \r\n", lu16_FlashID);



	/* Erase Sector: 0x000000 ~ 0x000FFF */
	IC_W25Qxx_EraseSector(0x000000);
	
	/* write read X1 mode */
	IC_W25Qxx_PageProgram(TxBufferI2C, 0x000000, 256);
	IC_W25Qxx_Read_Data(RxBufferI2C, 0x000000, 256);
	
	for (i = 0; i < 256; i++)
	{
		if (RxBufferI2C[i] != TxBufferI2C[i])
		{
	        co_printf("Error \r\n");
		}
	}

	IC_W25Qxx_QuadConfig(true);
	
	/* write read X4 mode */
	IC_W25Qxx_PageProgram_Quad(TxBufferI2C, 0x000100, 256);
	IC_W25Qxx_Read_Quad_Output(RxBufferI2C, 0x000100, 256);

	for (i = 0; i < 256; i++)
	{
		if (RxBufferI2C[i] != TxBufferI2C[i])
		{
	        co_printf("%02X Error \r\n", RxBufferI2C[i]);
		}
	}

	/* write read X4 mode */
	IC_W25Qxx_PageProgram_Quad(TxBufferI2C, 0x000200, 256);
	IC_W25Qxx_Read_Dual_Output(RxBufferI2C, 0x000200, 256);
	for (i = 0; i < 256; i++)
	{
		if (RxBufferI2C[i] != TxBufferI2C[i])
		{
	        co_printf("%02X Error \r\n", RxBufferI2C[i]);
		}
	}
	
	co_printf("SPI falsh demo end");
	
	while(1);
}

