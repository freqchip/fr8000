/*
  ******************************************************************************
  * @file    spi_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of SPI module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __SPI_DEMO_H__
#define __SPI_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/* spi_demo */
void spi_demo(void);

#endif
