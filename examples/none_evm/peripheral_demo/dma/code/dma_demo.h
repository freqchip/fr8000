/*
  ******************************************************************************
  * @file    dma_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of dma module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __DMA_DEMO_H__
#define __DMA_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* DMA demo select */
typedef enum
{
	DMA_M2M,                 /* memory to memory */
	DMA_M2P_UART,            /* memory to peripheral use uart */ 
	DMA_M2P_UART_IT,         /* memory to peripheral use uart with DMA interrupt*/ 
	DMA_M2P_UART_IT_LIST,    /* memory to peripheral use uart with DMA interrupt and list */ 
}enum_DMA_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* dma_demo */
void dma_demo(enum_DMA_Demo_t fe_Demo);

#endif
