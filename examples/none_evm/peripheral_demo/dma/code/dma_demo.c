/*
  ******************************************************************************
  * @file    dma_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   DMA module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "dma_demo.h"

#include "driver_gpio.h"
#include "driver_pmu.h"
#include "driver_dma.h"
#include "driver_uart_ex.h"

#include "co_printf.h"
#include "co_log.h"

#include "ble_stack.h"

GPIO_InitTypeDef GPIO_Handle;
DMA_HandleTypeDef  DMA_Chan0_Handle;
UART_HandleTypeDef Uart1_handle;

dma_LinkParameter_t LinkParameter;
DMA_LLI_InitTypeDef Link_Channel[10];

uint32_t gu32_BufferA[512];
uint32_t gu32_BufferB[512]; 

const uint8_t TextBuffer[] = 
{
"\r\n\
   上海富芮坤微电子有限公司成立于2014年，是一家致力于射频集成电路芯片的设计、研发及产品销售的\r\n集成电路设计的企业。\r\n \
   公司拥有一支综合素质高，实战能力强，实践经验丰富的技术研发团队。核心员工大多来自于清华、\r\n复旦、交大、哈工大等知名高校。\r\n \
   能够独立完成从射频、模拟、音频、协议栈、应用软件到应用产品开发的整体解决方案。\r\n \
   目前产品线主要为：蓝牙音频、低功耗蓝牙。现有产品有双模蓝牙音频SoC芯片和超低功耗（BLE）蓝牙 SoC芯片。\r\n \
   双模蓝牙音频SOC芯片主要应用于无线TWS蓝牙耳机、车载蓝牙以及智能穿戴手表等。\r\n \
   超低功耗（BLE）蓝牙 SoC芯片主要应用于智能穿戴、智能家居、运动器材、医疗健康、智能电表等。\r\n \
\r\n"
};

/*

初始化参数说明：

    typedef struct 
    {
        uint32_t Data_Flow;           ：数据流方向。可配置为：内存到内存，内存到外设，外设到内存

        uint32_t Request_ID;          ：请求ID。如果选择内存到外设，或者外设到内存的搬运方向。需要配置请求ID。
                                                如：UART1使用DAM发送数据，调用 __DMA_REQ_ID_UART1_TX(1) 来配置UART1发送请求使用1号ID
                                                    并设置Request_ID = 1; 将DMA通道绑定UART1 Tx。
        uint32_t Source_Inc;          ：源地址递增选择。

        uint32_t Desination_Inc;      ：目标地址递增选择。

        uint32_t Source_Width;        ：源数据位宽。

        uint32_t Desination_Width;    ：目标数据位宽。

    }dma_InitParameter_t;               

搬运长度计算案例：
    Data_Flow        = M2M;
    Request_ID       = 0;
    Source_Inc       = DMA_ADDR_INC_INC;
    Desination_Inc   = DMA_ADDR_INC_INC;
    Source_Width     = DMA_TRANSFER_WIDTH_32;（源数据位宽4   byte）
    Desination_Width = DMA_TRANSFER_WIDTH_32;（目标数据位宽4 byte）
    
    dma_start(xxx, xxx, xxx, 5, DMA_BURST_LEN_4, DMA_BURST_LEN_4);

    DMA传输的总长度为 5(Size) * 4(Source_Width) = 20 byte。
    DMA收到一次传输请求后，将待传输的数据项 4(DMA_BURST_LEN_4) * 4 byte = 16 byte的数据读取到 DMA FIFO中。再从FIFO中搬往目标地址。
    DMA再次收到一传输请求，将剩余的 20 – 16 = 4 byte的数据读取到 DMA FIFO中。再从FIFO中搬往目标地址。
    
    
    如上所示，搬运的总长度 = Size * 源数据位宽(Source_Width)。
              每次搬运的长度 = SrcBurstLen * 源数据位宽(Source_Width)。
              如果最终剩余的数据不足一次Burst搬运，则DMA将自动将剩余的数据取走。

    注意：每次搬运的长度(SrcBurstLen * Source_Width)不能超过DMA通道FIFO的深度。
          如果是内存与外设之间的传输，每次搬运的数据量也不能超过外设FIFO的深度。
*/

/************************************************************************************
 * @fn      dma_demo
 *
 * @brief   dma demo
 *
 * @param   fe_Demo: demo select. 
 */
void dma_demo(enum_DMA_Demo_t fe_Demo)
{
    uint32_t lu32_err = 0;

    __SYSTEM_DMA_CLK_ENABLE();

    switch(fe_Demo)
    {
        case DMA_M2M:
        {
            co_printf("DMA demo: memory to memory test \r\n");
            /*
                brief description:
                memory to memory test.
            */
            for (int i = 0; i < 512; i++)
            {
                gu32_BufferA[i] = (i << 24) | (i << 16) | (i << 8) | i;
                gu32_BufferB[i] = 0;
            }
            DMA_Chan0_Handle.Channel               = DMA_Channel0;
            DMA_Chan0_Handle.Init.Data_Flow        = DMA_M2M_DMAC;
            DMA_Chan0_Handle.Init.Request_ID       = 0;
            DMA_Chan0_Handle.Init.Source_Inc       = DMA_ADDR_INC_INC;    /* source address increase */
            DMA_Chan0_Handle.Init.Desination_Inc   = DMA_ADDR_INC_INC;    /* Desination address increase */
            DMA_Chan0_Handle.Init.Source_Width     = DMA_TRANSFER_WIDTH_32;    /* source     data transfer width 32bit */
            DMA_Chan0_Handle.Init.Desination_Width = DMA_TRANSFER_WIDTH_32;    /* Desination data transfer width 32bit */
            dma_init(&DMA_Chan0_Handle);

            dma_start(&DMA_Chan0_Handle, (uint32_t)gu32_BufferA, (uint32_t)gu32_BufferB, 512, DMA_BURST_LEN_1, DMA_BURST_LEN_1);
            while(!dma_get_tfr_Status(DMA_Channel0));
            dma_clear_tfr_Status(DMA_Channel0);
            
            for (int i = 0; i < 512; i++)
            {
                if (gu32_BufferA[i] != gu32_BufferB[i])
                {
                    lu32_err++;
                }
            }
            
            if (lu32_err)
            {
                co_printf("memory to memory test FAIL \r\n");     
            }
            else
            {
                co_printf("memory to memory test PASS \r\n");     
            }
        }break;

        case DMA_M2P_UART:
        {
            co_printf("DMA demo: memory to peripheral use uart test \r\n");
            /*
                brief description:
                memory to peripheral test.
            */
            
            /* init DMA */
            __DMA_REQ_ID_UART1_TX(1);
            DMA_Chan0_Handle.Channel               = DMA_Channel0;
            DMA_Chan0_Handle.Init.Data_Flow        = DMA_M2P_DMAC;
            DMA_Chan0_Handle.Init.Request_ID       = 1;
            DMA_Chan0_Handle.Init.Source_Inc       = DMA_ADDR_INC_INC;          /* source address increase */
            DMA_Chan0_Handle.Init.Desination_Inc   = DMA_ADDR_INC_NO_CHANGE;    /* Desination address no change */
            DMA_Chan0_Handle.Init.Source_Width     = DMA_TRANSFER_WIDTH_8;      /* source     data transfer width 8bit */
            DMA_Chan0_Handle.Init.Desination_Width = DMA_TRANSFER_WIDTH_8;      /* Desination data transfer width 8bit */
            dma_init(&DMA_Chan0_Handle);

            /* init GPIO Alternate Function */
            GPIO_Handle.Pin       = GPIO_PIN_2|GPIO_PIN_3;
            GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull      = GPIO_PULLUP;
            GPIO_Handle.Alternate = GPIO_FUNCTION_5;

            gpio_init(GPIO_A, &GPIO_Handle);

            /* init uart1 */
            __SYSTEM_UART1_CLK_ENABLE();

            Uart1_handle.UARTx = Uart1;
            Uart1_handle.Init.BaudRate   = 115200;
            Uart1_handle.Init.DataLength = UART_DATA_LENGTH_8BIT;
            Uart1_handle.Init.StopBits   = UART_STOPBITS_1;
            Uart1_handle.Init.Parity     = UART_PARITY_NONE;
            Uart1_handle.Init.FIFO_Mode  = UART_FIFO_ENABLE;

            uart_init_ex(&Uart1_handle);

            /* DMA Tx request threshold：TxFIFO empty */
            __UART_TxFIFO_THRESHOLD(((UART_HandleTypeDef *)&Uart1_handle), 0);
            /* uart1 transmit */
            dma_start(&DMA_Chan0_Handle, (uint32_t)TextBuffer, (uint32_t)&Uart1_handle.UARTx->DATA_DLL.DATA, sizeof(TextBuffer), DMA_BURST_LEN_16, DMA_BURST_LEN_16);
            while(!dma_get_tfr_Status(DMA_Channel0));
            dma_clear_tfr_Status(DMA_Channel0);

            co_printf("DMA demo: memory to peripheral use uart test END \r\n");
        }break;

        case DMA_M2P_UART_IT:
        {
            co_printf("DMA demo: memory to peripheral use uart with DMA interrupt test \r\n");
            /*
                brief description:
                memory to peripheral test with DMA interrupt.
            */
            
            /* init DMA */
            __DMA_REQ_ID_UART1_TX(1);
            DMA_Chan0_Handle.Channel               = DMA_Channel0;
            DMA_Chan0_Handle.Init.Data_Flow        = DMA_M2P_DMAC;
            DMA_Chan0_Handle.Init.Request_ID       = 1;
            DMA_Chan0_Handle.Init.Source_Inc       = DMA_ADDR_INC_INC;          /* source address increase */
            DMA_Chan0_Handle.Init.Desination_Inc   = DMA_ADDR_INC_NO_CHANGE;    /* Desination address no change */
            DMA_Chan0_Handle.Init.Source_Width     = DMA_TRANSFER_WIDTH_8;      /* source     data transfer width 8bit */
            DMA_Chan0_Handle.Init.Desination_Width = DMA_TRANSFER_WIDTH_8;      /* Desination data transfer width 8bit */
            dma_init(&DMA_Chan0_Handle);

            NVIC_ClearPendingIRQ(DMA_IRQn);
            NVIC_EnableIRQ(DMA_IRQn);

            /* init GPIO Alternate Function */
            GPIO_Handle.Pin       = GPIO_PIN_2|GPIO_PIN_3;
            GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull      = GPIO_PULLUP;
            GPIO_Handle.Alternate = GPIO_FUNCTION_5;

            gpio_init(GPIO_A, &GPIO_Handle);

            /* init uart1 */
            __SYSTEM_UART1_CLK_ENABLE();

            Uart1_handle.UARTx = Uart1;
            Uart1_handle.Init.BaudRate   = 115200;
            Uart1_handle.Init.DataLength = UART_DATA_LENGTH_8BIT;
            Uart1_handle.Init.StopBits   = UART_STOPBITS_1;
            Uart1_handle.Init.Parity     = UART_PARITY_NONE;
            Uart1_handle.Init.FIFO_Mode  = UART_FIFO_ENABLE;

            uart_init_ex(&Uart1_handle);

            /* DMA Tx request threshold：TxFIFO empty */
            __UART_TxFIFO_THRESHOLD(((UART_HandleTypeDef *)&Uart1_handle), 0);
            /* uart1 transmit */
            dma_start_IT(&DMA_Chan0_Handle, (uint32_t)TextBuffer, (uint32_t)&Uart1_handle.UARTx->DATA_DLL.DATA, sizeof(TextBuffer), DMA_BURST_LEN_16);
        }break;

        case DMA_M2P_UART_IT_LIST:
        {
            co_printf("DMA demo: memory to peripheral use uart with DMA interrupt test \r\n");
            /*
                brief description:
                memory to peripheral test with DMA interrupt and list.
            */

            __DMA_REQ_ID_UART1_TX(1);

            for (int i = 0; i < 10; i++)
            {
                LinkParameter.SrcAddr          = (uint32_t)TextBuffer;
                LinkParameter.DstAddr          = (uint32_t)&Uart1->DATA_DLL.DATA;
                LinkParameter.NextLink         = (uint32_t)&Link_Channel[i + 1];
                LinkParameter.Data_Flow        = DMA_M2P_DMAC;
                LinkParameter.Request_ID       = 1;
                LinkParameter.Source_Inc       = DMA_ADDR_INC_INC;
                LinkParameter.Desination_Inc   = DMA_ADDR_INC_NO_CHANGE;
                LinkParameter.Source_Width     = DMA_TRANSFER_WIDTH_8;
                LinkParameter.Desination_Width = DMA_TRANSFER_WIDTH_8;
                LinkParameter.Burst_Len        = DMA_BURST_LEN_16;
                LinkParameter.Size             = sizeof(TextBuffer);

                if (i == 9) 
                {
                    LinkParameter.NextLink = NULL;
                }

                dma_linked_list_init(&Link_Channel[i], &LinkParameter);
            }

            NVIC_ClearPendingIRQ(DMA_IRQn);
            NVIC_EnableIRQ(DMA_IRQn);

            /* init GPIO Alternate Function */
            GPIO_Handle.Pin       = GPIO_PIN_2|GPIO_PIN_3;
            GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull      = GPIO_PULLUP;
            GPIO_Handle.Alternate = GPIO_FUNCTION_5;

            gpio_init(GPIO_A, &GPIO_Handle);

            /* init uart1 */
            __SYSTEM_UART1_CLK_ENABLE();

            Uart1_handle.UARTx = Uart1;
            Uart1_handle.Init.BaudRate   = 115200;
            Uart1_handle.Init.DataLength = UART_DATA_LENGTH_8BIT;
            Uart1_handle.Init.StopBits   = UART_STOPBITS_1;
            Uart1_handle.Init.Parity     = UART_PARITY_NONE;
            Uart1_handle.Init.FIFO_Mode  = UART_FIFO_ENABLE;

            uart_init_ex(&Uart1_handle);

            /* DMA Tx request threshold：TxFIFO empty */
            __UART_TxFIFO_THRESHOLD(((UART_HandleTypeDef *)&Uart1_handle), 0);
            /* uart1 transmit */
            dma_linked_list_start_IT(Link_Channel, &LinkParameter, DMA_Channel0);
        }break;
    }

    while(1);
}

/************************************************************************************
 * @fn      dma_isr
 *
 * @brief   dma interrupt handler
 */
void dma_isr(void)
{
    if (dma_get_tfr_Status(DMA_Channel0))
    {
        dma_clear_tfr_Status(DMA_Channel0);

        co_printf("this is dma interrupt handler \r\n");
        co_printf("DMA demo: memory to peripheral use uart test END \r\n");
    }
}
