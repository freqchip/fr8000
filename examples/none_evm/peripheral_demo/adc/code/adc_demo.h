/*
  ******************************************************************************
  * @file    adc_demo.h
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   Header file of ADC module demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#ifndef __ADC_DEMO_H__
#define __ADC_DEMO_H__

#include <stdint.h>
#include <stdbool.h>

/* ADC demo select */
typedef enum
{
    ADC_SINGLE_CHANNEL,        // single-channel conversion
    ADC_MULTI_CHANNEL,         // Multi-channel conversion
    ADC_1_4VBAT,               // 1/4VBAT measure
    ADC_VBE,                   // VBE measure 
}enum_ADC_Demo_t;

/* Exported functions --------------------------------------------------------*/

/* adc_demo */
void adc_demo(enum_ADC_Demo_t adc_Demo);

#endif
