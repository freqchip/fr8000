/*
  ******************************************************************************
  * @file    adc_demo.c
  * @author  FreqChip Firmware Team
  * @version V1.0.0
  * @date    2021
  * @brief   ADC module Demo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 FreqChip.
  * All rights reserved.
  ******************************************************************************
*/
#include "adc_demo.h"

#include "driver_gpio.h"
#include "driver_pmu.h"
#include "driver_adc.h"
#include "driver_dma.h"

#include "co_printf.h"                                 
#include "co_log.h"

/************************************************************************************
 * @fn      adc_demo
 *
 * @brief   adc demo
 *
 * @param   fe_Demo: demo select. 

 */
void adc_demo(enum_ADC_Demo_t fe_Demo)
{
    GPIO_InitTypeDef    GPIO_Handle;
    adc_InitParameter_t ADC_InitParam;
    
    __SYSTEM_ADC_CLK_ENABLE();
    
    switch(fe_Demo)
    {
        /* single-channel conversion */
        case ADC_SINGLE_CHANNEL:
        {
            GPIO_Handle.Pin       = GPIO_PIN_7;
            GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull      = GPIO_PULLDOWN;
            GPIO_Handle.Alternate = GPIO_FUNCTION_8;
            gpio_init(GPIO_D, &GPIO_Handle);

            ADC_InitParam.ADC_CLK_DIV          = 5;
            ADC_InitParam.ADC_SetupDelay       = 80;
            ADC_InitParam.ADC_Reference        = ADC_REF_LDOIO;
            ADC_InitParam.FIFO_Enable          = FIFO_DISABLE;
            ADC_InitParam.FIFO_AlmostFullLevel = 10;

            adc_init(ADC_InitParam);

            adc_Channel_ConvertConfig(ADC_CHANNEL_0);

            adc_convert_enable();

            while(1)
            {
                if (adc_DataValid_status() == true)
                {
                    co_printf("ADC conversion value = %d \r\n", adc_get_data());
                }
            }
        }
        
        /* Multi-channel conversion */
        case ADC_MULTI_CHANNEL:
        {
            GPIO_Handle.Pin       = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
            GPIO_Handle.Mode      = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull      = GPIO_PULLDOWN;
            GPIO_Handle.Alternate = GPIO_FUNCTION_8;
            gpio_init(GPIO_D, &GPIO_Handle);
            
            ADC_InitParam.ADC_CLK_DIV    = 5;
            ADC_InitParam.ADC_SetupDelay = 80;
            ADC_InitParam.ADC_Reference  = ADC_REF_LDOIO;
            ADC_InitParam.FIFO_Enable    = FIFO_DISABLE;

            adc_init(ADC_InitParam);

            adc_Channel_ConvertConfig(ADC_CHANNEL_0|ADC_CHANNEL_1|ADC_CHANNEL_2|ADC_CHANNEL_3);

            adc_convert_enable();
            
            while(1)
            {
                for (int i = 0; i < 4; i++)
                {
                    co_printf("Channel[%d] = %d \r\n", i, adc_get_channel_data(i));
                }
            }
        }
        
        /* 1/4VBAT measure */
        case ADC_1_4VBAT:
        {
            ADC_InitParam.ADC_CLK_DIV    = 5;
            ADC_InitParam.ADC_SetupDelay = 80;
            ADC_InitParam.ADC_Reference  = ADC_REF_LDOIO;
            ADC_InitParam.FIFO_Enable    = FIFO_DISABLE;

            adc_init(ADC_InitParam);

            adc_VBAT_ConvertConfig();

            adc_convert_enable();
            
            while(1)
            {
                if (adc_DataValid_status() == true) 
                    co_printf("1/4 VBAT = %d \r\n", adc_get_data());
            }
        }
        
        /* VBE measure */
        case ADC_VBE:
        {
            ADC_InitParam.ADC_CLK_DIV    = 5;
            ADC_InitParam.ADC_SetupDelay = 80;
            ADC_InitParam.ADC_Reference  = ADC_REF_LDOIO;
            ADC_InitParam.FIFO_Enable    = FIFO_DISABLE;

            adc_init(ADC_InitParam);

            adc_VBE_ConvertConfig();

            adc_convert_enable();
            
            while(1)
            {
                if (adc_DataValid_status() == true) 
                    co_printf("VBE = %d \r\n", adc_get_data());
            }
        }
    }
}
