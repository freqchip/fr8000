#include <stdint.h>

#include "driver_pmu.h"
#include "driver_qspi.h"
#include "driver_flash.h"
#include "driver_uart.h"

#include "sys_utils.h"

/*according to 8010H */

uint8_t hex4bit_to_char(const uint8_t hex_value)
{
    if(hex_value < 10)
        return (hex_value + '0');
    else
        return ( (hex_value-10) + 'a');
    //return c_lib_hex2char[hex_value];
}
uint8_t hex4bit_to_caps_char(const uint8_t hex_value)
{
    if(hex_value < 10)
        return (hex_value + '0');
    else
        return ( (hex_value-10) + 'A');
    //return c_lib_hex2char[hex_value];
}

void hex_arr_to_str(const uint8_t hex_arr[],uint8_t arr_len,uint8_t *str)
{
    for(uint8_t i = 0; i < arr_len; i++)
    {
        co_sprintf((char *)(str+i*2),"%02X",hex_arr[i]);
    }
}

void str_to_hex_arr(const uint8_t *str, uint8_t hex_arr[],uint8_t arr_len)
{
    for(uint8_t i = 0; i < arr_len; i++)
    {
        hex_arr[i] = (uint8_t)str_to_val((const char *)(str+i*2),16,2);
        //   co_printf("asII:%x\r\n",addr[i]);
    }
}

void val_to_str(const uint32_t int_value,uint8_t *str)
{
    co_sprintf((char *)str,"%d",int_value);
}

/*-------------------------------------------------------------------------
    Function    :  char_to_val             ----add by chsheng, chsheng@accelsemi.com
    Return: -1=error
    Description:
        'a' -> 0xa  'A' -> 0xa
-------------------------------------------------------------------------*/
char char_to_val(const char c)
{
    if(c>='0' && c<='9')
        return c-'0';
    if((c>='a' && c<='f') || (c>='A' && c<='F'))
        return (c&0x7)+9;

    return (char)(-1);
}

/*-------------------------------------------------------------------------
    Function    :  str_to_val             ----add by chsheng, chsheng@accelsemi.com
    Return: -1=error
    Description:
        str = "123" bas = 10 return 123
        str = "123" bas = 16 return 0x123
-------------------------------------------------------------------------*/
int str_to_val( const char str[], char base, char n)
{
    int val = 0;
    char v;
    while(n != 0)
    {
        v = char_to_val(*str);
#if 0
        if (v == -1 || v >= base)
            return -1;
#else
        if (v == (char)(-1) || v >= base)
        {
            if(val == 0) //to filter abormal beginning and ending
            {
                str ++;
                n --;
                continue;
            }
            else
            {
                break;
            }
        }
#endif
        val = val*base + v;

        str++;
        n--;
    }
    return val;
}

/*-------------------------------------------------------------------------
    Function    :  ascii_char2val             ----add by chsheng, chsheng@accelsemi.com
    Return: -1=error
    Description:
        'a' -> 0xa  'A' -> 0xa
-------------------------------------------------------------------------*/
static char ascii_char2val(const char c)
{
    if(c>='0' && c<='9')
        return c-'0';
    if((c>='a' && c<='f') || (c>='A' && c<='F'))
        return (c&0x7)+9;

    return (char)(-1);
}

/*-------------------------------------------------------------------------
    Function    :  ascii_strn2val             ----add by chsheng, chsheng@accelsemi.com
    Return: -1=error
    Description:
        str = "123" bas = 10 return 123
        str = "123" bas = 16 return 0x123        
-------------------------------------------------------------------------*/
int ascii_strn2val( const char str[], char base, char n)
{
    int val = 0;
    char v;
    while(n != 0){
        v = ascii_char2val(*str);
#if 0
        if (v == -1 || v >= base) 
            return -1;
#else
        if (v == (char)(-1) || v >= base)
        {
            if(val == 0) //to filter abormal beginning and ending
            {
                str ++;
                n --;
                continue;
            }
            else
            {
                break;
            }
        }
#endif
        val = val*base + v;
        
        str++;
        n--;
    }
    return val;
}
